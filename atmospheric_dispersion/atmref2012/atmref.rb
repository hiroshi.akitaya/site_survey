#!/usr/local/bin/ruby

include Math

T0 = 273.15
P0 = 1013.25e2

Ts= 288.15
Ps= 1013.25e2

def n_m_1_s( l ) # n-1; l [nm]; A.Q. p. 262
  n_m_1_s = 1e-6*( 
                64.328 + 
                29498.1e-6/(146.0e-6-1.0/(l*l)) + 
                255.4e-6/(41.0e-6-1.0/(l*l) ))
  return n_m_1_s
end

def n_m_1( l, t, p )
  n_m_1 = n_m_1_s( l )*(p*Ts)/(Ps*t)
  return n_m_1
end

#def r_0( n )
#  r_0 = (n*n-1.0) / (2.0*n*n)
#  return r_0
#end

def r_0( l )
  n_m_1 = n_m_1( l, T0, P0 )
  r_0 = ( n_m_1*n_m_1 + 2.0*n_m_1 ) / 2.0 / (n_m_1*n_m_1 + 2.0*n_m_1 + 1.0 )
  return r_0
end

def r_n_0( l, zt )
  r_n_0 = r_0( l ) * Math.tan( degToRad( zt ) )
  return r_n_0
end

def r( l, zt, t, p )
  r = r_n_0( l, zt ) * ( p * T0 )/( P0 * t )
  return r
end

def radToArcsec( rad )
  arcsec = rad / Math::PI * 180.0 * 60.0 * 60.0
  return arcsec
end

def degToRad( deg )
  rad = deg/180.0*Math::PI
  return rad
end

l = ARGV[0].to_f

t = T0
p = P0


zt=0.0
while( zt <= 80.0 )
  n_m_1 = n_m_1( l, t, p )
  r_v = radToArcsec( r( 550.0, zt, t, p ) )
  r = radToArcsec( r( l, zt, t, p ) )
  dr = r - r_v
  printf("%6.2f %8.4f %10.6e %5.2f %6.2f\n", zt, l/1000.0,  n_m_1, r, dr )
  zt += 1.0
end

