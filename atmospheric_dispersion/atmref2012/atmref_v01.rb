#!/usr/local/bin/ruby

include Math

def n_m_1_s( l ) # n-1; l [nm]; A.Q. p. 262
  n_m_1 = 1e-6*( 
                64.328 + 
                29498.1e-6/(146.0e-6-1.0/(l*l)) + 
                255.4e-6/(41.0e-6-1.0/(l*l) ))
  return n_m_1
end

def n_m_1( l, t, p )
  ps = 1013.25e2
  ts = 288.15
  n_m_1 = n_m_1_s( l )*(p*ts)/(ps*t)
  return n_m_1
end

#def r_0( n )
#  r_0 = (n*n-1.0) / (2.0*n*n)
#  return r_0
#end

def r_0( n_m_1 )
  r_0 = ( n_m_1*n_m_1 + 2.0*n_m_1 ) / 2.0 / (n_m_1*n_m_1 + 2.0*n_m_1 + 1.0 )
  return r_0
end

def radToArcsec( rad )
  arcsec = rad / Math::PI * 180.0 * 60.0 * 60.0
  return arcsec
end

def degToRad( deg )
  rad = deg/180.0*Math::PI
  return rad
end

t0=273.15
p0=1013.25e2

l = ARGV[0].to_f

n_m_1 = n_m_1(l, t0, p0 )
r_0 = r_0( n_m_1 )
r_0_v = r_0( n_m_1( 550.0, t0, p0)  )

zt=0.0
while( zt <= 80.0 )
  printf("%6.2f %8.4f %10.6e %5.2f %6.2f\n", zt, l/1000.0,  n_m_1, radToArcsec( r_0 )*Math.tan( degToRad( zt ) ), radToArcsec( r_0 - r_0_v )* Math.tan( degToRad( zt ) ))
  zt += 1.0
end

